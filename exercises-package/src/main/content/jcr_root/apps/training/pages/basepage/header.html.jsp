<%@include file="/apps/training/global.jsp"%><%
%><sling:adaptTo adaptable="${currentPage}" adaptTo="net.distilledcode.training.NavigationMenu" var="menu"/><%
%><header>
    <nav>
        <ul><c:forEach var="child" items="${menu.navigationItems}">
            <li><a href="${child.path}.html">${child.title}</a></li>
        </c:forEach></ul>
    </nav>
</header>