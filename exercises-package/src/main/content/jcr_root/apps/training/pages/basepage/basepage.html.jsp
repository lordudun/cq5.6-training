<%@include file="/apps/training/global.jsp"%><%
%><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>${properties['jcr:title']}</title>
    <title>Forgot to set a title?</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link type="text/css" rel="stylesheet" href="${currentDesign.path}/static.css">
    <cq:include script="/libs/wcm/core/components/init/init.jsp"/>
</head>
<body>
    <cq:include script="header.html.jsp"/>
    <div class="content">
        <h1>${properties['jcr:title']}</h1>
        <cq:include script="content.html.jsp"/>
    </div>
    <cq:include script="footer.html.jsp"/>
</body>
</html>
