package net.distilledcode.training;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.PageManager;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;

public class Utils {

    public static Page getContainingPage(final Resource resource) {
        if (resource == null) {
            return null;
        }
        final ResourceResolver resolver = resource.getResourceResolver();
        final PageManager pageManager = resolver.adaptTo(PageManager.class);
        return pageManager != null ? pageManager.getContainingPage(resource) : null;
    }

    public static Page getParentPageByTemplate(final Page page, final String template) {
        if (page == null || template == null) {
            return null;
        }
        Page candidate = page;
        while (candidate != null && !template.equals(getTemplate(candidate))) {
            candidate = candidate.getParent();
        }
        return candidate;
    }

    private static String getTemplate(final Page page) {
        return page.getProperties().get("cq:template", "");
    }
}
