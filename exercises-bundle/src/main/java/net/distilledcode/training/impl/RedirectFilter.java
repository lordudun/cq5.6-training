package net.distilledcode.training.impl;

import com.day.cq.wcm.api.Page;
import com.day.cq.wcm.api.WCMMode;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.PropertyOption;
import org.apache.felix.scr.annotations.sling.SlingFilter;
import org.apache.felix.scr.annotations.sling.SlingFilterScope;
import org.apache.sling.api.SlingHttpServletRequest;
import org.apache.sling.api.SlingHttpServletResponse;
import org.apache.sling.api.resource.Resource;
import org.apache.sling.api.resource.ResourceResolver;
import org.apache.sling.api.resource.ValueMap;
import org.apache.sling.commons.osgi.PropertiesUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@SlingFilter(
        label = "%redirect.filter.label",
        description = "%redirect.filter.desc",
        metatype = true,
        scope = SlingFilterScope.REQUEST,
        order = -1999 // the WCMRequestFilter (order = -2000) sets the WCMMode
)
@SuppressWarnings("unused")
public class RedirectFilter implements Filter {

    private static final Logger LOG = LoggerFactory.getLogger(RedirectFilter.class);

    @Property(
            value = {"PREVIEW", "DISABLED"},
            options = {
                    @PropertyOption(name = "DESIGN", value = "Design"),
                    @PropertyOption(name = "DISABLED",  value = "Disabled"),
                    @PropertyOption(name = "EDIT",  value = "Edit"),
                    @PropertyOption(name = "PREVIEW", value = "Preview")
            }
    )
    private static final String WCMMODES_PROPERTY = "enabled.wcmmodes";

    private static final String[] DEFAULT_WCMMODES = {"PREVIEW", "DISABLED"};

    private Set<WCMMode> enabledWCMModes;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        if (request instanceof SlingHttpServletRequest
                && response instanceof SlingHttpServletResponse) {

            final SlingHttpServletRequest slingRequest = (SlingHttpServletRequest) request;
            final SlingHttpServletResponse slingResponse = (SlingHttpServletResponse) response;

            final String extension = slingRequest.getRequestPathInfo().getExtension();
            final Page page = slingRequest.getResource().adaptTo(Page.class);

            if (page != null && "html".equals(extension)) {
                final ResourceResolver resolver = slingRequest.getResourceResolver();
                final String redirectTarget = getValidRedirectTarget(resolver, page);
                final WCMMode wcmMode = WCMMode.fromRequest(request);

                if (redirectTarget != null && enabledWCMModes.contains(wcmMode)) {
                    LOG.info("Sending redirect from {} to {}", slingRequest.getPathInfo(), redirectTarget);
                    slingResponse.sendRedirect(redirectTarget);
                    return;
                }
                LOG.info("No redirect for {}, WCMMode={}", slingRequest.getPathInfo(), wcmMode);
            }
        }
        chain.doFilter(request, response);
    }

    private String getValidRedirectTarget(ResourceResolver resolver, Page page) {
        final ValueMap pageProperties = page.getProperties();
        final String redirectTarget = pageProperties.get("redirectTarget", "");
        final Resource resource = resolver.getResource(redirectTarget);
        if (resource != null && !"".equals(redirectTarget)) {
            return redirectTarget + ".html";
        }
        return null;
    }

    @SuppressWarnings("unused")
    @Activate
    private void activate(final Map<String, Object> config) {
        final String[] wcmModeNames = PropertiesUtil.toStringArray(config.get("wcmmodes"), DEFAULT_WCMMODES);
        final HashSet<WCMMode> wcmModes = new HashSet<WCMMode>();
        for (final String name : wcmModeNames) {
            wcmModes.add(WCMMode.valueOf(name));
        }
        this.enabledWCMModes = Collections.unmodifiableSet(wcmModes);
        LOG.info("Set WCMModes to {}", this.enabledWCMModes);
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // nothing to do
    }

    @Override
    public void destroy() {
        // nothing to do
    }
}
