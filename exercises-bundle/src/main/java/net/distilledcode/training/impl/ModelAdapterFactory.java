package net.distilledcode.training.impl;

import com.day.cq.wcm.api.Page;
import net.distilledcode.training.NavigationMenu;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Properties;
import org.apache.felix.scr.annotations.Property;
import org.apache.felix.scr.annotations.Service;
import org.apache.sling.api.adapter.AdapterFactory;
import org.apache.sling.api.resource.Resource;

@Component(metatype = false, immediate = false)
@Service
@Properties({
        @Property(name = AdapterFactory.ADAPTABLE_CLASSES, classValue = {
                Resource.class,
                Page.class
        }),
        @Property(name = AdapterFactory.ADAPTER_CLASSES, classValue = {
                NavigationMenu.class
        })
})
public class ModelAdapterFactory implements AdapterFactory {
    @Override
    public <AdapterType> AdapterType getAdapter(Object adaptable, Class<AdapterType> type) {
        if (adaptable == null || type == null) {
            return null;
        }

        if (adaptable instanceof Resource) {
            return adaptResource((Resource) adaptable, type);
        } else if (adaptable instanceof Page) {
            return adaptPage((Page) adaptable, type);
        }

        return null;
    }

    private <AdapterType> AdapterType adaptPage(final Page page, final Class<AdapterType> type) {
        if (NavigationMenu.class.isAssignableFrom(type)) {
            return (AdapterType) NavigationMenu.fromPage(page);
        }
        return null;
    }

    private <AdapterType> AdapterType adaptResource(final Resource resource, final Class<AdapterType> type) {
        if (NavigationMenu.class.isAssignableFrom(type)) {
            return (AdapterType) NavigationMenu.fromResource(resource);
        }
        return null;
    }
}
