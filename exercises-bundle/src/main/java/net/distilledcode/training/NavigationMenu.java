package net.distilledcode.training;

import com.day.cq.wcm.api.Page;
import org.apache.sling.api.resource.Resource;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class NavigationMenu {

    public static final String NAVIGATION_ROOT_TEMPLATE = "/apps/training/templates/homepage";

    private final Page rootPage;

    private NavigationMenu(final Page rootPage) {
        this.rootPage = rootPage;
    }

    public List<Page> getNavigationItems() {
        final ArrayList<Page> pages = new ArrayList<Page>();
        pages.add(rootPage);
        final Iterator<Page> children = rootPage.listChildren();
        while (children.hasNext()) {
            pages.add(children.next());
        }
        return pages;
    }

    public static NavigationMenu fromPage(final Page page) {
        final Page rootPage = Utils.getParentPageByTemplate(page, NAVIGATION_ROOT_TEMPLATE);
        return rootPage == null ? null : new NavigationMenu(rootPage);
    }

    public static NavigationMenu fromResource(final Resource resource) {
        return fromPage(Utils.getContainingPage(resource));
    }
}
